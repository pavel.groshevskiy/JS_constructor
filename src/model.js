import image from "./assets/image.jpg"
import {TitleBlock, TextBlock, ImageBlock, ColumnsBlock} from './classes/blocks'

const text = 'Lorem10 '
export const model = [
  new TitleBlock ('Hello from JS', {
      tag: 'h1',
      styles : {
      background: 'linear-gradient(to right, #ff0099, #493240)',
      color: '#fff',
      'text-align': 'center',
      padding: '1.5rem',
      'text-align': 'center'
    }
  }),
  new ImageBlock(image, {
    styles: {
      padding: '2rem 0',
      display: 'flex',
      'justify-content': 'center'
    },
    imageStyles: {
      width: '500px',
      height: 'auto'
    },
    alt: 'Катинка'
  }),
  new ColumnsBlock(['Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ducimus consequuntur, ad nisi, excepturi maiores culpa quibusdam, temporibus vitae in consectetur libero ut! Asperiores, vero. Reiciendis sit ipsum id fugit tempora?',
  'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ducimus consequuntur, ad nisi, excepturi maiores culpa quibusdam, temporibus vitae in consectetur libero ut! Asperiores, vero. Reiciendis sit ipsum id fugit tempora?',
  'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ducimus consequuntur, ad nisi, excepturi maiores culpa quibusdam, temporibus vitae in consectetur libero ut! Asperiores, vero. Reiciendis sit ipsum id fugit tempora?'], {
    styles: {
        background: 'linear-gradient(to right, #ff0099, #493240)',
        padding: '2rem',
        color: '#fff',
        'font-weight': 'bold'
    }
  }),
  new TextBlock(text, {
    styles: {
      background: 'linear-gradient(to left, #f2994a, #f2c94c)',
      padding: '1rem',
      'font-weight': 'bold'
    }
  })
]